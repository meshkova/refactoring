package ru.mea.refactoring;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс, демонстрирующий рефакторинг кода
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Refactoring {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите размер массива");
        int arraySize = scanner.nextInt();
        final int[] array = new int[arraySize];
        complection(array);
        System.out.println(Arrays.toString(array));

        search(array, arraySize);
    }

    /**
     * Метод, заплняющий массив рандомными значениями
     *
     * @param array массив
     */
    private static void complection(final int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = -25 + (int) (Math.random() * 51);
        }
    }

    /**
     * Метод, реализующий поиск индекса вводимого элемета
     *
     * @param array     массив
     * @param arraySize размер массива
     */
    private static void search(final int[] array, int arraySize) {
        System.out.println("Введите элемент, индекс которого хотите узнать");
        final int elementToSearch = scanner.nextInt();
        int number = -1;
        for (int i = 0; i < arraySize; i++) {
            if (array[i] == elementToSearch) {
                number = i;
                System.out.println("номер элемента в последовательности " + number);
                return;
            }
        }
        if (number == -1) System.out.println("нет такого значения");
    }
}
