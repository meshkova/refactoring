package ru.mea.search;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс, демонстрирующий линейный поиск
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class LinearSearch {
    private static final int MIN = -25;
    private static final int RANGE = 51;
    private static final String MESSAGE_LENGTH = "Введите размер массива ";
    private static final String MESSAGE_INDEX = "Введите значение элемента, индекс которого хотите узнать ";
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int arraySize = inputLengthOfArray(MESSAGE_LENGTH);
        int[] array = new int[arraySize];
        completion(array);
        System.out.println(Arrays.toString(array));

        int element = inputLengthOfArray(MESSAGE_INDEX);
        int index = search(array, element);
        resultOfSearch(index);
    }

    private static int inputLengthOfArray(String message) {
        System.out.println(message);
        return scanner.nextInt();
    }

    /**
     * Метод, заполняющий массив рандомными значениями от MIN до RANGE
     *
     * @param array массив
     */
    private static void completion(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN + (int) (Math.random() * RANGE);
        }
    }

    /**
     * Метод, реализущий поиск элемента в массиве
     *
     * @param array           массив
     * @param elementToSearch элемент, индекс которого нужно найти
     * @return индекс элемента или -1 если такого элеметнта нет
     */
    private static int search(int[] array, int elementToSearch) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == elementToSearch) {

                return i;

            }
        }
        return -1;
    }

    /**
     * Метод, выводящий результат
     *
     * @param index результат поиска предыдущего метода
     */
    private static void resultOfSearch(int index) {
        System.out.println((index == -1) ? "Нет такого значения " : "Номер элемента в последовательности " + index);
    }
}
